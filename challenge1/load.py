
import pandas as pd

from os import getenv
from sqlalchemy import create_engine
from sqlalchemy.engine import Engine


def connect(password: str, user: str = 'postgres', db_name: str = 'dw_flights') -> Engine:
    engine = create_engine(f"postgresql+psycopg2://{user}:{password}@localhost/{db_name}")
    return engine


def load_csv(base_dir: str, tables: dict, ordering: tuple, db: Engine) -> None:
    
    for table in ordering:
        col_name_map = tables[table]

        df = pd.read_csv(f'{base_dir}/nyc_{table}.csv', header=0)
        if col_name_map and isinstance(col_name_map, dict):
            df = df.rename(columns=col_name_map)
        
        print(df.head())
        input('PRESS ENTER')
        
        columns_to_remove = {'Unnamed', 'Unnamed: 0'}
        
        df = df.drop(list(columns_to_remove), axis=1, errors='ignore')
        
        if table == 'weather':
            df = df.drop_duplicates(subset=['origin', 'year', 'month', 'day', 'hour'], keep='last')
        
        df.to_sql(name=table, con=db, if_exists='append', index=False)
    


def main():
    tables = {
            'airlines': None,
            'airports': {
                'faa': 'faa',
                'name': 'name',
                'lat': 'latitude',
                'lon': 'longitude',
                'alt': 'altitude',
                'tz': 'timezone',
                'dst': 'dst',
                'tzone': 'timezone_name'
            },
            'flights': {
                'arr_time': 'actual_arr_time',
                'dep_time': 'actual_dep_time'
            },
            'planes': None,
            'weather': None
    }

    pwd: str = getenv('POSTGRES_PASSWORD')
    db: Engine = connect(pwd)

    base_dir: str = 'dataset/'
    
    ordering = ('airlines', 'airports', 'planes', 'flights', 'weather')
    load_csv(base_dir, tables, ordering, db)


if __name__ == '__main__':
    main()

